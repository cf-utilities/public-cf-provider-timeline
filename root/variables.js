var baseFormat = {
  y: 0,
  align: "center",
  arrowcolor: "rgba(0, 0, 0, 0)",
  ax: -0,
  bgcolor: "rgba(255, 0, 255, .25)",
  bordercolor: "rgba(0, 0, 0, 0)",
  borderpad: 3,
  borderwidth: 0,
  font: {size: 14},
  textangle: 0,
  yref: 'y'
}

var baseDateFormat = {
  y: 0,
  align: "center",
  arrowcolor: "rgba(0, 0, 0, 0.25)",
  arrowhead: 5,
  arrowsize: 1,
  ax: -0,
  bgcolor: "rgba(0, 255, 0, 0.25)",
  bordercolor: "rgba(0, 0, 0, 0)",
  borderpad: 3,
  borderwidth: 0,
  font: {size: 10},
  textangle: 0,
  yref: 'y'
}